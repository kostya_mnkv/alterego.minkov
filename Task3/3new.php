<?php
header('Content-Type: text/html; charset=utf-8');

$array = getArrayDate();

//showArray(bubbleSort($array)); // bubble sort
//showArray(sort2($array)); // rsort

showArray(sort3($array)); // array_multisort
showArray(sort4($array)); //usort

showArray(sort5($array)); //uasort

function cmp($a, $b)
{
    if ($a[1] == $b[1]) {
        return 0;
    }
    return ($a[1] > $b[1]) ? -1 : 1;
}


function sort5($array){
    uasort($array, "cmp");
    return $array;
}

function sort3 ($array){
    $arr = [];
    foreach ($array as $key){
        $arr[] = $key[1];
    }
    array_multisort($arr, SORT_DESC,$array);
    return $array;
}

function sort4($array){
    usort($array, "cmp");
    return $array;
}


function sort2 ($array){
    $arr = array(
        array()
    );
/////////////////////////////////////////////////////////////////
    $i = 0;
    foreach ($array as $key){
        for($j = 0; $j < 3; $j++) {
            $arr[$i][0] = $key[1];
            $arr[$i][1] = $key[0];
            $arr[$i][2] = $key[2];
        }
        $i++;
    }
/////////////////////////////////////////////////////////////////
    rsort($arr);
////////////////////////////////////////////////////////////////
    $i = 0;
    foreach ($arr as $key){
        for($j = 0; $j < 3; $j++) {
            $arr[$i][0] = $key[1];
            $arr[$i][1] = $key[0];
            $arr[$i][2] = $key[2];
        }
        $i++;
    }
///////////////////////////////////////////////////////////////
    return $arr;
}



function bubbleSort($arr) {
    $size = sizeof($arr)-1;
    for ($i=0; $i<$size; $i++) {
        for ($j=0; $j<$size-$i; $j++) {
            $k = $j+1;
            if ($arr[$k][1] > $arr[$j][1]) {
                list($arr[$j], $arr[$k]) = array($arr[$k], $arr[$j]);
            }
        }
    }
    return $arr;
}

function getArrayDate(){
    $array = [];

    $file = fopen("dates.csv", "r");
//Зчитуєм файл
    while (!feof($file)) {
        $array[] = fgetcsv($file, 2024, ";");
    }
    fclose($file);


//Створюєм масив зчитаних даних
    foreach ($array as $key){
        $list[] = explode(',',$key[0]);
    }

    return $list;
}
function showArray($array){
    echo '<p>[Дата] : [Число] : [Имя]</p>';
    foreach ($array as $key){
        echo '<p>[ '. toUTF($key[0]). ' ] : [ ' .toUTF($key[1]) . ' ] : [ ' . toUTF($key[2]). ' ]</p>';
    }
    echo "<hr>";
}

function toUTF($text){
    return iconv( "windows-1251", "utf-8",$text);
}
function debug($array){
    echo '<hr><pre>';
    echo var_dump($array);
    echo '</pre><hr>';
}