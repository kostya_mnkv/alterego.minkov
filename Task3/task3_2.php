<?php
header('Content-Type: text/html; charset=windows-1251');

$array_sum = [];
$list = [];
if (file_exists("dates.csv")) {
//-------------------------------------------Зчитали файл-------------------------------------------------------------//
    $file = fopen("dates.csv", "r");
    while (!feof($file)) {
        $list[] = fgetcsv($file, 2024, ",");
    }
    fclose($file);
//--------------------------------------------------------------------------------------------------------------------//
    // Додаємо значення та перевіряємо ідентичність імен
    foreach ($list as $key) {
        if ($key == "") continue; // пуста строка
        $key[2] = str_replace(" ","",$key[2]);// Якщо в імені
        if (preg_match('/[0-9]/', $key[2])) {
            $key[2] =  preg_replace('/([0-9])/', '$11', $key[2]);
            toUTF($array_sum[trim($key[2])] += $key[1]);
        } else {
            toUTF($array_sum[$key[2]] += $key[1]);
        }
    }

    debug($array_sum);

//--------------------------------------------Створюю масив для запису------------------------------------------------//
    $list = [];
    $index = 0;
    foreach ($array_sum as $key => $val){
        $list[$index][0] = $key;
        $list[$index][1] = $val;
        $index++;
    }
//-------------------------------------------------Записую у файл-----------------------------------------------------//
    $file = fopen('names_sum.csv', 'w');
    foreach ($list as $field) {
        fputcsv($file, $field);
    }
    fclose($file);
}else{
    echo 'Файла dates.csv не існує';
}
//--------------------------------------------------------------------------------------------------------------------//
function toUTF($text){ return iconv( "windows-1251", "utf-8",$text);}
//--------------------------------------------------------------------------------------------------------------------//
function debug($array){
    echo '<hr><pre>';
    echo var_dump($array);
    echo '</pre><hr>';
}