<?php

$dates = get_array_date();

show_array(sort1($dates));
echo "<hr>";
show_array(sort2($dates));
echo "<hr>";
show_array(sort3($dates));


//-------------------------------------------------------------------------------------------------//
function show_array($array){
    /*foreach ($array as $item){
        echo $item.'<br>';
    }*/
    echo "<pre>";
    var_dump($array);
    echo "</pre>";
}
//-------------------------------------------------------------------------------------------------//
function sort1 ($array){
    array_multisort($array, SORT_DESC);
    return $array;
}
//-------------------------------------------------------------------------------------------------//
function sort2 ($array){
    function cmp($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return ($a > $b) ? -1 : 1;
    }
    uasort($array, 'cmp');

    return $array;
}
//-------------------------------------------------------------------------------------------------//
function sort3 ($array){
    arsort($array);
    return $array;
}

//-------------------------------------------------------------------------------------------------//
function get_array_date(){
    //Обробляємо файл csv та отримуємо тільки поля із датою
    $array = [];
    $file = fopen("dates.csv", "r");
    $i = 0;
//Проходимо до кінця
    while (!feof($file)) {
        $array[] = fgetcsv($file, 2024, ";");
    }

    //Зупиняємо на 1 для того щоб не взяти у масив поле Дата та 91 для того щоб не звяти булевську змінну
        foreach ($array as $key){
            $i++;
            if($i == 91) break;
            if($i == 1) continue;
    //створюємо масив даних
            $val = implode($key);
            $list[] = explode(',',$val);
        }

        $arr_date = [];
        $size = sizeof($list);
    //Створюємо масив із датами
        for($i = 0; $i < $size; $i++){
            $arr_date[] = $list[$i][0];
        }
    return $arr_date;
}
//-------------------------------------------------------------------------------------------------//
function toUTF($text){
    return iconv( "windows-1251", "utf-8",$text);
}