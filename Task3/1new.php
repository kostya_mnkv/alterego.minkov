<?php
header('Content-Type: text/html; charset=utf-8');

$names = ["Иван 1", "Иван 2", "Олег", "Дмитрий"];
$dates = []; // Для дат
$list = array(); // Для збереження даних та запису у файл

//--------------------------------------------------------------------------------------------------------------------//
//Для перевірки дати
$date1 = new DateTime();
$date2 = new DateTime();
$date2->modify('+8 month'); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

//--------------------------------------------------------------------------------------------------------------------//
//Підраховуємо кількість днів
$countDay = 0; // Для кількості ітерацій в циклі перевірки дати
$count = 0;

while ($date2 != $date1) {
    $countDay++;
    $date1->modify('+1 day');
}

//Скидуємо дату до нашого дня
$date1->modify("-$countDay days");

//Скидуємо дату до початку нашого місяця
$b = $date1->format('d');
$b++;
$date1->modify("-$b days");
$monday =0;

//--------------------------------------------------------------------------------------------------------------------//

//Дати для запису у файл
$dateWrite = new DateTime();
$dateAudit = new DateTime();
$dateAudit->modify('+8 month');//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1


//------------------------------------------------------------------------------------------///////////////

//--------------------------------------------------------------------------------------------------------------------//
//Перевірка 1 та 3 понеділків у проміжку +3 місяця та запис їх у масив dates
for ($i = 1; $i < $countDay; $i++) {
    //День понеділок то
    if($date1->format('D') == 'Mon'){
        $monday++;
        if ($monday == 1 || $monday == 3){
            //Додавання елементів у масив
                $dates[] = toWin1251($date1->format('d-m-Y'));
        }
    }
    if($date1->format('d') == $date1->format('t')){
        $monday = 0;
        $count = 0;
    }
    //Опаньки 3 понеділок, очистка
    if($monday == 3) {
        $monday = 0;

        $thisMonth = $date1->format('t');
        $thisMonth = (int)($thisMonth) - $count;

        $date1->modify("+$thisMonth days");
        $count = 0;
    }
    $count++;
    $date1->modify('+1 day');
}
//--------------------------------------------------------------------------------------------------------------------//
$audit_date = [];
$index = 0;//Підрахунок комірок записів
// Запис даних у масив даних ліст
while ($dateWrite->format('d:m:Y') != $dateAudit->format('d:m:Y')){
    foreach ($dates as $val){
        if($val == $dateWrite->format('d-m-Y')){
            for ($j = 0; $j < 3; $j++) {
                switch ($j) {
                    case 0:
                        $audit_date[] = toWin1251($dateWrite->format('d-m-Y'));
                        $list[$index][$j] = toWin1251($dateWrite->format('d-m-Y'));
                        break;
                    case 1:
                        $list[$index][$j] = toWin1251(rand(100, 999));
                        break;
                    case 2:
                        $list[$index][$j] = toWin1251($names[rand(0, 3)]);
                        break;
                }
            }
        }
    }
    $index++;
    $dateWrite->modify('+1 day');
}
//--------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------Запис-----------------------------------------------------------------//

if(file_exists('dates.csv')) {
    $i = 0;
    $array = [];
    $final_array = [];
    $file = fopen("dates.csv", "r");

    //Зчитуєм дані із файлу
    while (!feof($file)) {
        $array[] = fgetcsv($file, 2024, ";");
    }

    //Створюємо масив із даних у файлі
    foreach ($array as $key => $value) {
        $final_array[] = explode(',', $value[0]);
    }

    $check = false;
    //Перевірка недостающие даты
    for($i = 0; $i < sizeof($audit_date); $i++) {
        if($audit_date[$i] != $final_array[$i][0]) {
            $file = fopen('dates.csv', 'w');
                foreach ($list as $fields) {
                    fputcsv($file, $fields);
                }
            fclose($file);
            $check = true;
        }
    }
    if($check == true) echo 'Дані оновлено';
    else echo 'Нових дат немає';
}else{
    $file = fopen('dates.csv', 'w');
//Цикл для запису даних із полів масива у файл
    foreach ($list as $fields) {
        fputcsv($file, $fields);
    }
    fclose($file);
    echo 'Запис пройшов успішно';
}
//--------------------------------------------------------------------------------------------------------------------//
//Перевід у 1251
function toWin1251($text){
    return iconv( "utf-8", "windows-1251",$text);
}
//--------------------------------------------------------------------------------------------------------------------//




















