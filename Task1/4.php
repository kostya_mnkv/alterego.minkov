<?php
require_once "my_func.php";

    //Створюємо масив із введеного тексту
    $array = explode(" ", $_POST['text']);
    //Перебираєм масив та перевіряєм слова які мають більше одного символа
    $size = sizeof($array);

    foreach ($array as $key => $val){
        if(mb_strlen($array[$key],'UTF-8') > 1){
            $array[$key] = mb_ucfirst(mb_strtolower($array[$key],'UTF-8')); // перший символ у верхньому
        }else{
            $array[$key] = mb_strtolower($array[$key],'UTF-8');
        }
    }

    //Переводимо масив в строку
    $text = implode("_", $array);
?>


<meta charset="UTF-8">

<form action="4.php" method="post">
    <textarea style="color: #0013ff" name="text" cols="50" rows="5" autofocus><? echo $_POST['text'] ?></textarea>
    <br><br>
    <input type="submit" name="sent">
    <br><br>
    <textarea style="color: #e33900" cols="50" rows="5" readonly><? echo $text?></textarea>
</form>
