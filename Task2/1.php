<?php

header('Content-Type: image/png');
header('Content-Disposition: attachment; filename=download.png');
header('Content-Description: File Transfer');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize('img.png'));
readfile('img.png');
